window.onload = function() {
  animateBanner();
  setTimeout( animateBanner, 15000);
};

function animateBanner() {

    var banner = document.getElementById('banner');
    var button = document.getElementById('button');
    var whiteBlock = document.getElementById('white-block');

    var slidesArr = new Array(
      document.getElementById('slide-1'),
      document.getElementById('slide-2'),
      document.getElementById('slide-3'),
      document.getElementById('slide-4')
    );

    var showSlide = function(slideIndex) {

      banner.className = (slideIndex == 1 || slideIndex == 3) ? 'red-bg' : 'orange-bg';
      button.className = (slideIndex == 1 || slideIndex == 3) ? 'red-bg' : 'orange-bg';
      whiteBlock.className = (slideIndex > 1) ? '' : 'small';

      for(var i=0; i<slidesArr.length; i++) {
        slidesArr[i].className = (i == slideIndex) ? 'opacity-1 ' : 'opacity-0 ';
      }
    };

    var slideTime = 3500;
    var currentSlide = 1;

    var changeSlideInterval = setInterval(function() {
      if(currentSlide >= slidesArr.length) {
        clearInterval(changeSlideInterval);
      }
      else {
        showSlide(currentSlide++);
      }
    }, slideTime);

    showSlide(0);
}
